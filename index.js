module.exports = (
    settings, // Microservices settings
    routes, // Routes array to be included into the app.
    before = [], // Middlewares array to be included before the routes.
    after = [] // Middlewares array to be included after the routes.
) => {

    require('module-alias/register')
    global.settings = settings;
    global.settings.msHost = 'http://ms01.easysupp.it';
    
    const express = require('express');
    const cookieParser = require('cookie-parser');
    const i18n = require('i18n');
    const msclient = require('ms-client')
    const middlewares = require('gateway-commons/middlewares')
    const _ = require('underscore');

    global.lang = settings.defaultLang;

    var app = express();
    i18n.configure({
        locales: ['en', 'it'],
        directory: __dirname + '/i18n',
        syncFiles: true,
        objectNotation: true,
        register: global,
        defaultLocale: settings.defaultLang
    });
    
    app.use(express.json());
    app.use(express.urlencoded({
        extended: false
    }));
    app.use(cookieParser());
    app.use(middlewares.lang);

    app.use(middlewares.logging);
    app.use(async (req, res, next) => {
        var publics = ['/public'];
        // We check all of the requests that are not placed in a /public 
        // path for a token.

        var protected = true;
        publics.forEach( p => { if(req.path.indexOf(p) != -1) protected = false });
        if(protected && !req.headers.token) return next(new Error(__("Missing 'token' header, perform authentication.")));
        
        next();
    });

    app.use( (req, res, next) => {
        req.ms = new msclient(req.headers.token);
        next();
    });

    before.forEach((m) => {
        app.use(m);
    });

    app.use(routes);
    app.get('/ping', (req, res, next) => {
        res.send({
            data: req.ms.contract
        });
    });

    after.forEach((m) => {
        app.use(m);
    });

    app.use(middlewares.messages);

    return app;
}